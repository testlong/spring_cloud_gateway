package com.test.demo.server1.web;

import com.test.demo.server1.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/01/07 19:07
 */

@Controller
@RequestMapping("demo")
public class DemoController {

    //返回数据
    @GetMapping("test1")
    @ResponseBody
    public String test1(HttpServletRequest request) {
        System.out.println(request.getRequestURI()); // /demo/test1
        return "返回数据";
    }


    //返回页面
    @GetMapping("test2")
    public String test2(HttpServletRequest request) {
        System.out.println(request.getRequestURI()); // /demo/test2
        return "test";
    }

    //返回对象
    @GetMapping("test3")
    @ResponseBody
    public User test3(HttpServletRequest request) {
        System.out.println(request.getRequestURI()); // /demo/test3
        User user = new User();
        user.setId(1);
        user.setName("admin");
        return user;
    }


}