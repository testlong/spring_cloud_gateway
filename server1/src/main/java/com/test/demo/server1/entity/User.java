package com.test.demo.server1.entity;

import java.io.Serializable;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/01/07 19:06
 */

public class User implements Serializable {

    private Integer id;

    private String name;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}